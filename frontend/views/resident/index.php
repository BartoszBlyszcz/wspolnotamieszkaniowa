<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
$this->title = "Wspólnota 3.0.0";
$this->registerJsFile('@web/frontend/assets/js/resident.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
?>
<div class="layer">
  <?php $form = ActiveForm::begin(['id' => 'save-form' ]) ?>
  	<div class="title">
  		Wspólnota Mieszkaniowa Trójka
	</div>

  	<div class="resident">
        <?=$resident['first_name']?> <?=$resident['last_name']?><br/>
        Lokal: <?=$resident['staircase']?>/<?=$resident['flat']?>
	</div>

	<table class="resident-option">
		<tr>
			<th>Powierzchnia [m<sup>2</sup>]</th>
			<td><?= $form->field($model, 'area')->textInput(['type'=>'number', 'value'=>$resident['area'], 'min'=>0, 'step'=>'0.01'])->label(false) ?></td>
			<th>Woda na osobę [m]</th>
			<td><?= $form->field($model, 'water_person')->textInput(['type'=>'number', 'value'=>$resident['water_person'], 'min'=>0, 'step'=>'0.01'])->label(false) ?></td>
		</tr> 
		<tr>
			<th>Lokatorzy</th>
			<td><?= $form->field($model, 'residents')->textInput(['type'=>'number', 'value'=>$resident['residents'], 'min'=>0, 'step'=>'1'])->label(false) ?></td>
			<th>Czynsz za m<sup>2 [zł]</sup></th>
			<td><?= $form->field($model, 'rental')->textInput(['type'=>'number', 'value'=>$resident['rental'], 'min'=>0, 'step'=>'0.01'])->label(false) ?></td>
		</tr> 
		<tr>
			<th>Woda/Ścieki [zł]</th>
			<td><?= $form->field($model, 'sewage')->textInput(['type'=>'number', 'value'=>$resident['sewage'], 'min'=>0, 'step'=>'0.01'])->label(false) ?></td>
			<th>Kominiarz [zł]</th>
			<td><?= $form->field($model, 'sweep')->textInput(['type'=>'number', 'value'=>$resident['sweep'], 'min'=>0, 'step'=>'0.01'])->label(false) ?></td>
		</tr> 
		<tr>
			<th>Energia Wspólna [zł]</th>
			<td><?= $form->field($model, 'energy')->textInput(['type'=>'number', 'value'=>$resident['energy'], 'min'=>0, 'step'=>'0.01'])->label(false) ?></td>
			<th>Deratyzacja [zł]</th>
			<td><?= $form->field($model, 'disinfestation')->textInput(['type'=>'number', 'value'=>$resident['disinfestation'], 'min'=>0, 'step'=>'0.01'])->label(false) ?></td>
		</tr>
		<tr>
			<th>Śmieci [zł]</th>
			<td><?= $form->field($model, 'rubbish')->textInput(['type'=>'number', 'value'=>$resident['rubbish'], 'min'=>0, 'step'=>'0.01'])->label(false) ?></td>
			<th>Konserwacja terenów Wspólnoty [zł]</th>
			<td><?= $form->field($model, 'maintenance')->textInput(['type'=>'number', 'value'=>$resident['maintenance'], 'min'=>0, 'step'=>'0.01'])->label(false) ?></td>
		</tr> 
		<tr>
			<th>Licznik Wody: Początek Roku </th>
			<td><?= $form->field($model, 'w_counter_start')->textInput(['type'=>'number', 'value'=>$resident['w_counter_start'], 'min'=>0, 'step'=>'1'])->label(false) ?></td>
			<th>Licznik Wody: Koniec Roku</th>
			<td><?= $form->field($model, 'w_counter_end')->textInput(['type'=>'number', 'value'=>$resident['w_counter_end'], 'min'=>0, 'step'=>'1'])->label(false) ?></td>
		</tr>
		<tr>
			<th>Suma </th>
			<td><input type="number" id='sum' class='form-control' value="" readonly/></td>
			<th>Dług: </th>
			<td><?= $form->field($model, 'debt')->textInput(['type'=>'number', 'value'=>$resident['debt'], 'min'=>0, 'step'=>'0.01'])->label(false) ?></td>
		</tr>  
	</table>


        <?php /*
  <div class="row">
	  <div class="col-md-6">
	  	
	  </div>
	  <div class="col-md-6">
	  	<?= $form->field($model, 'flat')->textInput(['type'=>'number', 'value'=>0, 'min'=>0])->label('Mieszkanie') ?>
	  </div>
  </div>*/ ?>
  <div class="form-group">
      <?= Html::submitButton('Zapisz', ['class' => 'btn btn-success btn-save', 'name' => 'save-button']) ?>
  </div>

<?php ActiveForm::end(); ?>
</div>