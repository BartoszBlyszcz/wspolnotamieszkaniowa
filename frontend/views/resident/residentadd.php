<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
$this->title = "Wspólnota 3.0.0";
?>
<div class="layer">
  <?php $form = ActiveForm::begin(['id' => 'save-form']); ?>
  	<div class="row">
  		<div class="col-md-12">
  			<?= $form->field($model, 'first_name')->textInput(['autofocus' => true])->label('Imię') ?>
  		</div>
	</div>
  	<div class="row">
  		<div class="col-md-12">
  			<?= $form->field($model, 'last_name')->textInput()->label('Nazwisko') ?>
  		</div>
	</div>
  <div class="row">
	  <div class="col-md-6">
	  	<?= $form->field($model, 'staircase')->textInput(['type'=>'number', 'value'=>0, 'min'=>0])->label('Klatka') ?>
	  </div>
	  <div class="col-md-6">
	  	<?= $form->field($model, 'flat')->textInput(['type'=>'number', 'value'=>0, 'min'=>0])->label('Mieszkanie') ?>
	  </div>
  </div>
  <div class="form-group">
      <?= Html::submitButton('Dodaj', ['class' => 'btn btn-primary btn-add', 'name' => 'save-button']) ?>
  </div>

<?php ActiveForm::end(); ?>
</div>