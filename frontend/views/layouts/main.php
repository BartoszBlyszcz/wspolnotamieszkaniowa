<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;
use common\models\Residents;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <link href="https://fonts.googleapis.com/css?family=Hanalei+Fill|Lato:400,700,900&amp;subset=latin-ext" rel="stylesheet">
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<div class="site">
    <div class="cont">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
    <div class="menu">
        <h2>Mieszkańcy</h2>
        <?php 
            $residents = Residents::getAll();
            foreach ($residents as $key => $value) {
                ?>
                  <a class='btn' href="/resident/<?=$value['resident_id']?>"><?=$value['first_name']?> <?=$value['last_name']?> <?=$value['staircase']?>/<?=$value['flat']?></a>
                <?php
            }
        ?>
        <div class="line"></div>
      <a class='btn' href="/resident/add">+ Dodaj Mieszkańca</a>            
    </div>
</div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
