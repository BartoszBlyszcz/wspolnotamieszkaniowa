<?php
namespace frontend\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\Residents;
use common\models\ConstM;
use common\models\Flat;
/**
 * Site controller
 */
class ResidentController extends Controller
{

	public function actionIndex(){
		$model = new Flat();
		$get = Yii::$app->request->get();
		$post = Yii::$app->request->post();

		if(!isset($get['rid'])) return $this->redirect("/site/index");
		if($model->load($post)){
			if($model->save()){
				return $this->refresh();
			}
		}

		$resident = Flat::getData($get['rid']);
		return $this->render('index', ['model'=> $model, 'resident'=>$resident]);
	}

	public function actionAdd(){
		$model = new Residents();
		$post = Yii::$app->request->post();
		$postData = $model->load($post);
		if($postData){
			if($model->save()){
				return $this->refresh();
			}
		}
		else{
			return $this->render('residentadd', ['model'=>$model]);
		}
	}

}
?>