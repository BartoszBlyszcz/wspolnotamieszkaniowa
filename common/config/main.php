<?php
return [
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
         'urlManager' => [
                'class' => 'yii\web\UrlManager',
                'showScriptName' => false,  // Disable index.php
                'enablePrettyUrl' => true,  // Disable r= routes
                'enableStrictParsing' => true,
                'rules' => array(
                    '' => 'site/index',
                    '/' => 'site/index',
                    '<module>/<controller>/<action>/<id:\d+>' => '<module>/<controller>/<action>',
                    '/resident/<rid:\d+>' => '/resident/index',
                    '/resident/add' => '/resident/add',
                ),

        ],
    ],
];
