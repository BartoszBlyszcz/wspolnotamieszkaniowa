<?php
namespace common\models;

use Yii;
use yii\base\Model;

class Allerts extends Model {
    public static function setAllert($info, $type){
        $type = strtoupper($type);
        
        $session = Yii::$app->session;
        if(!$session->isActive){
            $session->open();
        }

        if($type == 'SUCCESS') $session['offertSuccess'] = true;
        else if($type == 'DANGER')$session['offertAllert'] = true;
        
        $session['info']= $info;
    }

    public static function getAllert(){

         
        $session = Yii::$app->session;
        if(!$session->isActive){
            $session->open();
        }
        
        if($session['offertSuccess']){
            echo "<div class='alert alert-success' role='alert'>
            {$session['info']}
          </div>";
          $session['offertSuccess'] = null;
          unset($session['offertSuccess']);
        }
        else if($session['offertAllert']){
            echo "<div class='alert alert-danger' role='alert'>
            {$session['info']}
          </div>";
          $session['offertAllert'] = null;
          unset($session['offertAllert']);
        }
        
        $session['info'] = null;
    }

}
