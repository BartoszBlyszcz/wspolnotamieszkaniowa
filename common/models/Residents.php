<?php
namespace common\models;

use Yii;
use yii\base\Model;

/**
 * Login form
 */
class Residents extends Model{

	public $first_name;
	public $last_name;
	public $staircase;
	public $flat;

	  public function rules()
    {
      return [
	      [['flat', 'staircase', 'last_name', 'first_name'], 'required'],
      ];
    }

	public static function getAll(){
		$sql = "SELECT * FROM residents";
		$ret = Yii::$app->db->createCommand($sql)->queryAll();
		if(count($ret) > 0) return $ret;
		else return [];
	}

	public function save(){
		if($this->validate()){
			$sql = "INSERT INTO residents VALUES (NULL, '$this->first_name', '$this->last_name', '$this->staircase', '$this->flat')";
			Yii::$app->db->createCommand($sql)->execute();
			return true;
		}
		else return false;
	}
}
?>