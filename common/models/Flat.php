<?php
namespace common\models;

use Yii;
use yii\base\Model;

/**
 * Login form
 */
class Flat extends Model{

	public $area;
	public $residents;
	public $rental;
	public $sewage;
	public $rubbish;
	public $debt;
	public $w_counter_start;
	public $w_counter_end;
	public $water;
	public $maintenance;
	public $water_person;
	public $disinfestation;
	public $energy;

	  public function rules()
    {
      return [
	      [['area', 'residents', 'rental', 'sewage', 'rubbish', 'debt', 'w_counter_start', 'w_counter_end', 'water', 'maintenance', 'water_person', 'disinfestation', 'energy'], 'required'],
      ];
    }

	public static function getData($id){
        $sql = "SELECT * FROM flat WHERE resident_id = $id";
        if(count(Yii::$app->db->createCommand($sql)->queryAll()) == 0){
            $sql = "INSERT INTO flat (resident_id) VALUES ($id)";
            Yii::$app->db->createCommand($sql)->execute();
        }

		$sql = "SELECT * FROM flat INNER JOIN residents USING(resident_id) WHERE resident_id = $id";
		$ret = Yii::$app->db->createCommand($sql)->queryAll();
		if(count($ret) > 0) return $ret[0];
		else return [];
	}

	public function save(){
        /*
		if($this->validate()){
			$sql = "INSERT INTO residents VALUES (NULL, '$this->first_name', '$this->last_name', '$this->staircase', '$this->flat')";
			Yii::$app->db->createCommand($sql)->execute();
			return true;
		}
		else return false;*/
	}
}
?>